#variable "nginx_count" {
#  type = number
#  default = "2"
#}
#
#variable "secret_key" {
#  type = string
#  default = "Undefined environment variabble $TF_VARS_secret_key"
#}
#
#variable "project_id" {
#  type = string
#  default = "Undefined environment variabble $TF_VARS_project_id"
#}
#
#variable "region" {
#  type = string
#  default = "Undefined environment variabble $TF_VARS_region"
#}
#
#variable "ami_ubuntu_18_04_lts" {
#  type = string
#  default = "Undefined environment variabble $TF_VARS_ami_ubuntu_18_04_lts"
#}
#
#variable "instance_type" {
#  type = string
#  default = "Undefined environment variabble $TF_VARS_instance_type"
#}


variable "project_id" {
  default = "kubernetes-315301"
  description = "The project ID to host the cluster"
}
variable "cluster_name" {
  description = "The name for the GKE cluster"
  default     = "learnk8s-cluster"
}
variable "env_name" {
  description = "The environment for the GKE cluster"
  default     = "dev"
}
variable "region" {
  description = "The region to host the cluster in"
  default     = "asia-southeast1"
}
variable "network" {
  description = "The VPC network created to host the cluster in"
  default     = "gke-network"
}
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "10.40.0.0/16"
}
variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "10.41.0.0/16"
}
variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "10.42.0.0/16"
}