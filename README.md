jvN8UCP3EN_zTThzHsS5
4Dw7-D1BP8WyGAyhqys4
git config --global credential.helper store
git remote set-url origin https://



# set up gitlab runner

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo yum install gitlab-runner

# Command to register runner
sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941EUJbZFdKPZw7wsEsAjpm

# Setup master node

Khởi tạo cluster:

kubeadm init --pod-network-cidr=10.244.0.0/16 >> cluster_initialized.txt

Tạo thư mục .kube ở thư mục home.

mkdir ~/.kube

Copy admin.conf vào thư mục .kube vừa tạo ở trên

cp /etc/kubernetes/admin.conf ~/.kube/config

Verify master node:

kubectl get node -o wide


Cài đặt pod network (network plugin): https://github.com/coreos/flannel

Deploying flannel:

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml


Disable TCP checksum offloading for flannel. reboot (optional)

# Edit file: vim /etc/rc.local
# Add line:
ethtool -K eth0 tso off
# reboot

# Setup worker nodes

Run command join cluster: command join cluster ở file cluster_initialized.txt (master node) theo format bên dưới
kubeadm token create --print-join-command

kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>

Verify cluster: SSH vào master node:

kubectl get node -o wide


Connect đến cluster từ local.
Copy file ~/.kube/config ở master node về local.

kubectl get node -o wide

Fix lỗi coredns không thể start.
https://github.com/kubernetes/kubeadm/issues/193#issuecomment-330060848
https://github.com/coredns/coredns/issues/2494

systemctl stop kubelet
systemctl stop docker
iptables --flush
iptables -tnat --flush
systemctl start kubelet
systemctl start docker


# deploy dashboard
sudo mkdir certs
sudo chmod 777 -R certs
openssl req -nodes -newkey rsa:2048 -keyout certs/dashboard.key -out certs/dashboard.csr -subj "/C=/ST=/L=/O=/OU=/CN=kubernetes-dashboard"
openssl x509 -req -sha256 -days 365 -in certs/dashboard.csr -signkey certs/dashboard.key -out certs/dashboard.crt
kubectl create secret generic kubernetes-dashboard-certs --from-file=certs -n kubernetes-dashboard
kubectl apply -f https://raw.githubusercontent.com/dungnv0811/cloud-k8s-infra/master/dashboard/dashboard.yml
kubectl apply -f https://raw.githubusercontent.com/dungnv0811/cloud-k8s-infra/master/dashboard/admin-user.yml
# get token dashboard
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
