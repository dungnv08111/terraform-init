#!/bin/bash

echo 'dvnguyen:123456' | sudo chpasswd

sudo yum check-update -y
sudo yum -y install openssl
curl -fsSL https://get.docker.com/ | sh
sudo systemctl start docker
sudo systemctl enable docker


sudo setenforce 0
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo systemctl mask firewalld
sudo yum install -y iptables-services
sudo systemctl start iptables
sudo systemctl enable iptables
sudo iptables -I INPUT 4 -s 10.148.0.0/24 -j ACCEPT
sudo service iptables save

cat << EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl -p  /etc/sysctl.d/k8s.conf

cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF


sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable --now kubelet
sudo rm /etc/containerd/config.toml
sudo systemctl restart containerd