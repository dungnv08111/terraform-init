// configuration
provider "google" {
  project     = "kubernetes-315301"
  region      = "asia-southeast1-a"
  credentials = file("google-cloud-official-credentials.json")
}

resource "google_compute_instance" "k8s-master" {
  name         = "k8s-master"
  machine_type = "e2-medium"
  zone         = "asia-southeast1-a"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  tags = ["http-server", "https-server"]

  metadata = {
    ssh-keys = "dvnguyen:${file("id_rsa.pub")}"
  }

  metadata_startup_script = file("k8s-master.sh")
}

resource "google_compute_instance" "k8s-worker1" {
  name         = "k8s-worker1"
  machine_type = "e2-medium"
  zone         = "asia-southeast1-a"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  tags = ["http-server", "https-server"]

  metadata = {
    ssh-keys = "dvnguyen:${file("id_rsa.pub")}"
  }

  metadata_startup_script = file("k8s-worker.sh")
}

resource "google_compute_instance" "k8s-worker2" {
  name         = "k8s-worker2"
  machine_type = "e2-medium"
  zone         = "asia-southeast1-a"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  tags = ["http-server", "https-server"]

  metadata = {
    ssh-keys = "dvnguyen:${file("id_rsa.pub")}"
  }

  metadata_startup_script = file("k8s-worker.sh")
}
